from happyfuntokenizing import Tokenizer

if __name__ == "__main__":
    tok = Tokenizer(preserve_case=False)
    sample = {
        "I am pusheen the cat",
        "meow meow give me food"
    }
    a = []
    for line in sample:
        tokenized = tok.tokenize(line)
        a = a + tokenized
    if "pusheen" in a:
        print "pusheen in a"

