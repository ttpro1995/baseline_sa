from happyfuntokenizing import Tokenizer
from nltk.corpus import stopwords
import os
import string
import time
import random
punctuation = set(string.punctuation)


if __name__ == "__main__":
    start_time = time.time()
    alpha = 1

    # TOKENIZATION step
    cachedStopWords = stopwords.words("english")
    tok = Tokenizer(preserve_case=False)
    root_neg ="./txt_sentoken/neg"
    root_pos = "./txt_sentoken/pos"
    neg_dir = os.listdir(root_neg)
    pos_dir = os.listdir(root_pos)
    neg_docs = []
    pos_docs = []
    vocabulary =[]

    print 'check1'

    for d in neg_dir:
        f = open(root_neg+"/"+d);
        tokenized = tok.tokenize(f.read())
        tokenized = list(set(tokenized))  # remove dublicate
        tokenized = [word for word in tokenized if word not in cachedStopWords] # remove stopword
        tokenized = [word for word in tokenized if word not in punctuation] # remove punctuation


        vocabulary+= tokenized
        neg_docs.append(tokenized)

    print 'check2'

    for d in pos_dir:
        f = open(root_pos+"/"+d);
        tokenized = tok.tokenize(f.read())
        tokenized = list(set(tokenized))  # remove dublicate
        tokenized = [word for word in tokenized if word not in cachedStopWords]
        tokenized = [word for word in tokenized if word not in punctuation]

        vocabulary += tokenized
        pos_docs.append(tokenized)

    print 'check3'

    vocabulary = list(set(vocabulary)) # remove dublicate

    print 'check4'

    # get the subset of vocabulary
    sub_vocabulary = random.sample(vocabulary,100);

    # Boolean Multinomial Naive Bayes
    n_neg_doc = len(neg_dir)
    n_pos_doc = len(pos_dir)
    n_doc = n_neg_doc+n_pos_doc

    p_neg = float(n_neg_doc) / n_doc # P(negative)
    p_pos = float(n_pos_doc) / n_doc # P(positive)

    print p_neg
    print p_pos

    text_pos = []
    text_neg = []
    for doc in neg_docs:
        text_neg +=doc
    for doc in pos_docs:
        text_pos +=doc

    p_w_pos = {}  # p(w/positive)
    p_w_neg = {}  # p(w/negative)

    for word in sub_vocabulary:
        c_pos = text_pos.count(word)
        c_neg = text_neg.count(word)
        p_w_pos[word] = float(c_pos + alpha) / (c_pos + c_neg +  alpha*len(sub_vocabulary))
        p_w_neg[word] = float(c_neg + alpha) / (c_pos + c_neg + alpha* len(sub_vocabulary))

    print 'meow'
    print("--- %s seconds ---" % (time.time() - start_time))